# Optimisation
## Projet : Mixed-QAP

## <br> Développeurs

- [Maxime Bulteel](https://gitlab.com/Mawime)
- [Arthur Ringot](https://gitlab.com/aringot1612)

## Description du projet :

Disponible [ici](https://www.overleaf.com/read/phjcbfrmfycr).

## Instances de données :

Le répertoire **./instances/** contient l'ensemble des instances de données utilisées pour le problème Mixed-QAP.

## Architecture projet :

langage choisi : **Java**.

Le code est disponible dans le répertoire **./src/** et est séparé en 5 packages distincts :

- algo, contient tous les algorithmes utilisés et une classe abstraite nommée **Search**.
- evaluation, contient :
  - Toutes les méthodes d'évaluation de solutions : **MixedQAPEval** et **IncrementalEval** ;
  - Toutes les méthodes d'évaluation de voisinage : **FullNeighborhoodEval** et **IncrementalNeighborhoodEval**, héritées de **NeighBorHoodEval** ;
  - Une classe **Solution** symbolisant une solution au problème Mixed-QAP.
- operator, contient :
  - Une classe **UniformContinue** permettant d'initialiser une solution ;
  - Une classe **RandomPermutation** permettant de réaliser une permutation aléatoire dans une solution ;
  - Une classe **KSwap** permettant de réaliser **k** permutation(s) aléatoire(s) dans une solution ;
  - Une classe **Simulation** permettant de tester un ensemble d'algorithmes sur un ensemble d'instances ;
  - Une classe **Runner** regroupant les instances de classes utilisées par tous les exécutables.
- executables, contient :
  - Une classe **HillClimbingRunner** permettant d'exécuter un algorithme Hill Climbing ;
  - Une classe **ILSRunner** permettant d'exécuter un algorithme ILS ;
  - Une classe **MixedILSRunner** permettant d'exécuter un algorithme ILS avec optimisation continue ;
  - Une classe **RandomSearchRunner** permettant d'exécuter un algorithme Random Search ;
  - Une classe **OneAndOneESRunner** permettant d'exécuter un algorithme 1 + 1 ES ;
  - Une classe **SimulationRunner** permettant d'exécuter une simulation d'algorithmes complète.
- deprecated, contient :
  - Une classe **IncrementalEvalImproved** créée pour mettre en place l'évaluation incrémentale "Dérivée seconde", non fonctionnelle.

Finalement, seules les classes du package **executables** sont réellement utilisables par l'utilisateur.

Avec un IDE tel qu’IntelliJ, il est très simple d'exécuter le programme de son choix.

Sans IDE, vous pouvez tenter les opérations suivantes.

Il est important d'utiliser une Invite de Commande et de se placer dans le répertoire du projet.

## Comment compiler le code :

### Linux / MacOS

    find -name "*.java" > sources.txt
    mkdir out
    javac -d out @sources.txt

### Windows

    dir /s /B *.java > sources.txt
    mkdir out
    javac -d out @sources.txt

## Comment exécuter le code :

### La simulation :
    java -cp ./out/ executables.SimulationRunner

### L'algorithme Random Search :
    java -cp ./out/ executables.RandomSearchRunner

### L'algorithme Hill Climbing :
    java -cp ./out/ executables.HillClimbingRunner

### L'algorithme ILS :
    java -cp ./out/ executables.ILSRunner

### L'algorithme Mixed ILS :
    java -cp ./out/ executables.MixedILSRunner

### L'algorithme 1 + 1 ES :
    java -cp ./out/ executables.OneAndOneESRunner

## <br> Rapport de projet :

[Accessible ici](https://aringot1612.gitlab.io/projet-mixed-qap/rapport.pdf)

## <br> Documentation technique :

[Accessible ici](https://aringot1612.gitlab.io/projet-mixed-qap/)