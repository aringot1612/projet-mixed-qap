package evaluation;

/** Classe prenant en charge l'évaluation de solution incrémentale. */
public class IncrementalEval {
    /** Instance de la méthode d'évaluation complète, obligatoire pour le fonctionnement de cette méthode d'évaluation. */
    private final MixedQAPeval eval;
    /** Stocke une somme de valeurs issue de la formule appliquée. */
    private double sum;
    /** (u-v) -> Permutation et k -> Index pour les boucles for. */
    private int u, v, k;

    /** Constructeur
     *
     * @param eval L'instance d'évaluation complète à utiliser.
     */
    public IncrementalEval(MixedQAPeval eval){
        this.eval = eval;
        this.u = 0;
        this.v = 0;
        this.sum = 0.0;
        this.k = 0;
    }

    /** Cette méthode permet d'évaluer une solution issue du voisinage d'une solution initiale.
     *
     * Pour que cette évaluation fonctionne correctement,
     * Il faut impérativement fournir la solution initiale, et non le voisin.
     * Il faut également fournir la permutation de solution responsable du voisin à évaluer.
     *
     * @param solution Solution initiale à utiliser pour évaluer la solution voisine.
     * @param move Permutation qui, appliquée à la solution initiale, donne le voisin à évaluer.
     */
    public void apply(Solution solution, int[] move) {
        // Inversion u-v si u > v.
        if(move[0] < move[1]){
            u = move[0];
            v = move[1];
        }
        else{
            u = move[1];
            v = move[0];
        }
        sum = 0.0;
        for(k = 0 ; k < u ; k++){
            sum += ((solution.flow[u][k] - solution.flow[v][k])*(this.eval.distance[solution.sigma[v]][solution.sigma[k]] - this.eval.distance[solution.sigma[u]][solution.sigma[k]]));
        }
        for(k = u+1 ; k < v ; k++){
            sum += ((solution.flow[k][u] - solution.flow[v][k])*(this.eval.distance[solution.sigma[v]][solution.sigma[k]] - this.eval.distance[solution.sigma[u]][solution.sigma[k]]));
        }
        for(k = v+1 ; k < this.eval.n ; k++) {
            sum += ((solution.flow[k][u] - solution.flow[k][v]) * (this.eval.distance[solution.sigma[v]][solution.sigma[k]] - this.eval.distance[solution.sigma[u]][solution.sigma[k]]));
        }
        solution.fitness = solution.fitness + (2 * sum);
    }
}