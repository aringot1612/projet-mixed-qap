package evaluation;

/******************************************************

 Author:
 Sebastien Verel,
 Univ. du Littoral Côte d'Opale, France.
 version 0 : 2020/12/01

 Evaluation function of the Mixed-variable QAP

 ******************************************************/

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

/** Classe prenant en charge l'évaluation de solution complète. */
public class MixedQAPeval {
    // dimension of the problem
    public int n;

    // distance matrix
    public double [] [] distance;

    // flow matrices : flow = flow0 + flow1 * xi + flow2 * xj + flow3 xi * xj
    public double [] [] flow0;
    public double [] [] flow1;
    public double [] [] flow2;
    public double [] [] flow3;

    /*
       constructor
       becareful : any validation verication during the parsing of the file

       @param fileInstanceName name of the knapsack instance
     */
    public MixedQAPeval(String fileInstanceName) throws FileNotFoundException {
        readInstance(fileInstanceName);
    }

    // evaluation function
    public void apply(Solution solution) {
        solution.fitness = 0.0;
        if (solution.modifiedX) {
            // when x is modified, compute the personnal flow
            for(int i = 1; i < n; i++) {
                for(int j = 0; j < i; j++) {
                    solution.flow[i][j] = flow0[i][j] + flow1[i][j] * solution.x[i]+ flow2[i][j] * solution.x[j] + flow3[i][j] * solution.x[i] * solution.x[j];
                    if (solution.flow[i][j] > 0)
                        solution.fitness += solution.flow[i][j] * distance[ solution.sigma[i] ][ solution.sigma[j] ];
                    else
                        solution.flow[i][j] = 0;
                }
            }
            solution.modifiedX = false;
        } else {
            // when x is not modified
            for(int i = 1; i < n; i++) {
                for(int j = 0; j < i; j++) {
//                    if (solution.flow[i][j] > 0)
                    solution.fitness += solution.flow[i][j] * distance[ solution.sigma[i] ][ solution.sigma[j] ];
                }
            }
        }
        solution.fitness *= 2;  // by symmetry
    }

    public String toString() {
        String s = "" + this.n + '\n' ;

        s += printMatrix(this.distance);
        s += '\n' ;

        s += printMatrix(this.flow0);
        s += '\n' ;
        s += printMatrix(this.flow1);
        s += '\n' ;
        s += printMatrix(this.flow2);
        s += '\n' ;
        s += printMatrix(this.flow3);

        return s;
    }

    protected void readInstance(String fileInstanceName) throws FileNotFoundException {
        File file = new File(fileInstanceName);
        Scanner sc = new Scanner(file);

        // read the number of objects
        n = sc.nextInt();

        distance = readMatrix(sc);
        flow0 = readMatrix(sc);
        flow1 = readMatrix(sc);
        flow2 = readMatrix(sc);
        flow3 = readMatrix(sc);

        sc.close();
    }

    protected double[][] readMatrix(Scanner sc){
        double [] [] m = new double[n][n];

        for(int i = 0; i < this.n; i++) {
            for(int j = 0; j < this.n; j++) {
                m[i][j] = sc.nextInt();
            }
        }
        return m;
    }

    protected String printMatrix(double [] [] m) {
        String s = "" ;

        for(int i = 0; i < this.n; i++) {
            s += m[i][0];
            for(int j = 1; j < this.n; j++) {
                s += " " + m[i][j];
            }
            s += '\n';
        }
        return s;
    }
}