package evaluation;

/** Classe abstraite prenant en charge les évaluations de voisinage. */
public abstract class NeighBorHoodEval {
    public NeighBorHoodEval(){}
    abstract public void init(Solution solution, double[][] delta);
    abstract public void update(Solution solution, int[] move, double[][] delta);
}