package evaluation;

/** Classe prenant en charge l'évaluation de voisinage incrémentale. */
public class IncrementalNeighborhoodEval extends NeighBorHoodEval{
    /** Instance permettant d'évaluer tout un voisinage en utilisant une évaluation de fitness par incrémentation. */
    private final IncrementalEval eval;
    /** Stocke la fitness de solution initiale. */
    private double currentFitness;

    /** Constructeur.
     *
     * @param eval Instance d'une évaluation de fitness par variation.
     */
    public IncrementalNeighborhoodEval(IncrementalEval eval){
        this.eval = eval;
        this.currentFitness = 0.0;
    }

    /** Cette méthode évalue tout un voisinage de solution en utilisant une méthode d'évaluation incrémentale.
     *
     * @param solution La solution initiale, c'est le voisinage de cette solution que l'on recherche à évaluer.
     * @param delta Paramètre qui permettra de stocker l'ensemble des variations pour tout le voisinage.
     */
    @Override
    public void init(Solution solution, double[][] delta) {
        // Sauvegarde de la fitness - solution initiale.
        this.currentFitness = solution.fitness;
        // Pour chaque permutation / voisin possible pour une solution...
        for(int i = 1 ; i < solution.sigma.length ; i++){
            for(int j = 0; j < i ; j++){
                // La fitness de notre solution est ré-initialisée.
                solution.fitness = currentFitness;
                // Evaluation de la nouvelle solution / voisin.
                eval.apply(solution, new int[]{j, i});
                // On stocke la variation de fitness pour ce voisin.
                delta[i][j] = solution.fitness - currentFitness;
            }
        }
        solution.fitness = currentFitness;
    }

    @Override
    public void update(Solution solution, int[] move, double[][] delta) {init(solution, delta);}
}