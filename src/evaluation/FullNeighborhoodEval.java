package evaluation;

/** Classe prenant en charge l'évaluation de voisinage complète. */
public class FullNeighborhoodEval extends NeighBorHoodEval{
    /** Instance permettant d'évaluer tout un voisinage en utilisant une évaluation de fitness. */
    private final MixedQAPeval eval;
    /** Stocke la fitness de solution initiale. */
    private double currentFitness;

    /** Constructeur.
     *
     * @param mixedQAPeval Instance d'une évaluation de fitness.
     */
    public FullNeighborhoodEval(MixedQAPeval mixedQAPeval){
        this.eval = mixedQAPeval;
        this.currentFitness = 0.0;
    }

    /** Cette méthode évalue tout un voisinage de solution en utilisant une méthode d'évaluation complète.
     *
     * @param solution La solution initiale, c'est le voisinage de cette solution que l'on recherche à évaluer.
     * @param delta Paramètre qui permettra de stocker l'ensemble des variations pour tout le voisinage.
     */
    @Override
    public void init(Solution solution, double[][] delta) {
        // Sauvegarde de la fitness - solution initiale.
        currentFitness = solution.fitness;
        // Pour chaque permutation / voisin possible pour une solution...
        for(int i = 1 ; i < solution.sigma.length ; i++){
            for(int j = 0; j < i ; j++){
                // La solution devient temporairement le voisin ciblé par la permutation.
                solution.swap(i, j);
                // Evaluation de la nouvelle solution / voisin.
                eval.apply(solution);
                // On stocke la variation de fitness pour ce voisin.
                delta[i][j] = solution.fitness - currentFitness;
                // On réalise de nouveau un swap : la solution redevient la solution initiale.
                solution.swap(i, j);
            }
        }
        solution.fitness = currentFitness;
    }

    @Override
    public void update(Solution solution, int[] move, double[][] delta) {init(solution, delta);}
}