package algo;

import evaluation.NeighBorHoodEval;
import evaluation.Solution;
import operator.KSwap;

/** Classe prenant en charge l'algorithme ILS. */
public class ILS extends Search{
    /** Instance de LocalSearch permettant d'effectuer une recherche locale dans une solution. */
    private final LocalSearch localSearch;
    /** Instance de l'opérateur KSwap permettant de faire des permutations multiples sur une solution. */
    private final KSwap perturbation;
    /** Sauvegarde une liste sigma, permettant de garder la meilleure liste rencontrée. */
    private final int[] sigmaBackup;
    /** Sauvegarde la meilleure fitness rencontrée. */
    private double bestFitness;

    /** Constructeur
     *
     * @param neighBorHoodEval Le type d'évaluation de voisinage. Attention, seules les instances de FullNeighborhoodEval et IncrementalNeighborhoodEval sont admises.
     * @param n La dimension de la solution.
     * @param time Le temps d'exécution max admis pour l'algorithme.
     * @param kSwap L'instance de KSwap permettant de réaliser la perturbation de l'ILS.
     */
    public ILS(NeighBorHoodEval neighBorHoodEval, int n, long time, KSwap kSwap){
        this.localSearch = new LocalSearch(neighBorHoodEval, n);
        this.n = n;
        this.time = time;
        this.perturbation = kSwap;
        this.sigmaBackup = new int[this.n];
        this.bestFitness = 0;
    }

    /** Permet de minimiser la fitness d'une solution en utilisant un algorithme Iterated Local Search.
     *
     * @param solution la solution à minimiser.
     *
     * @return La nouvelle solution minimisée.
     */
    @Override
    public Solution compute(Solution solution) {
        // Initialisation
        initOccur();
        // Application d'une recherche locale sur la solution.
        this.localSearch.compute(solution);
        this.maxTime = System.currentTimeMillis() + this.time;
        // Sauvegarde de la solution, on considère qu'il s'agit, pour le moment, de la meilleure.
        saveSolution(solution);
        // Tant que le temps maximum n'est pas encore atteint...
        while (System.currentTimeMillis() < this.maxTime){
            // Lancement d'une perturbation aléatoire sur la solution.
            perturbation.run(solution);
            // Recherche locale sur la solution clonée.
            this.localSearch.compute(solution);
            // Si la meilleure solution fournie par la recherche locale est meilleure que la solution initiale...
            if (solution.fitness < this.bestFitness)
                // On sauvegarde la nouvelle meilleure solution.
                saveSolution(solution);
            else
                // Sinon on récupère la meilleure solution enregistrée.
                restoreSolution(solution);
            occur++;
        }
        return solution;
    }

    /** Cette méthode permet de sauvegarder l'état d'une solution fournie en paramètre.
     *
     * Elle enregistre la liste sigma de la solution donnée dans une liste spécifique.
     * Elle se charge également de sauvegarder la fitness.
     *
     * @param solution La solution à restaurer.
     */
    private void saveSolution(Solution solution){
        if (n >= 0) System.arraycopy(solution.sigma, 0, sigmaBackup, 0, n);
        this.bestFitness = solution.fitness;
    }

    /** Cette méthode permet de restaurer une solution fournie en paramètre.
     *
     * Elle restaure la liste des sigma en utilisant la liste de backup.
     * Elle restaure également la fitness.
     *
     * @param solution La solution nécessitant une restauration.
     */
    private void restoreSolution(Solution solution){
        if (n >= 0) System.arraycopy(sigmaBackup, 0, solution.sigma, 0, n);
        solution.fitness =  this.bestFitness;
    }
}