package algo;

import evaluation.NeighBorHoodEval;
import evaluation.Solution;

/** Classe prenant en charge l'algorithme Local Search. */
public class LocalSearch extends Search{
    /** Le type d'évaluation de voisinage à utiliser. */
    private final NeighBorHoodEval neighBorHoodEval;

    /** Constructeur
     *
     * @param neighBorHoodEval Le type d'évaluation à voisinage complet à utiliser. Attention, seules les instances de FullNeighborhoodEval et IncrementalNeighborhoodEval sont admises.
     * @param n La dimension de la solution.
     */
    public LocalSearch(NeighBorHoodEval neighBorHoodEval, int n){
        this.neighBorHoodEval = neighBorHoodEval;
        this.n = n;
        this.delta = new double[this.n][this.n];
    }

    /** Permet de minimiser la fitness d'une solution en utilisant un algorithme de Recherche Locale.
     * Cet algorithme est utilisé exclusivement par l'algorithme Iterated Local Search.
     *
     * @param solution la solution à minimiser.
     *
     * @return La nouvelle solution minimisée.
     */
    @Override
    public Solution compute(Solution solution) {
        // Analyse du voisinage. */
        this.neighBorHoodEval.init(solution, this.delta);
        // Cette méthode permet de récupérer le meilleur mouvement afin d'obtenir le meilleur voisin.
        selectBestNeighbor();
        // Si le voisin est meilleur que la solution initiale...
        if(delta[move[0]][move[1]] < 0){
            // La solution devient son meilleur voisin via ce swap.
            solution.swap(move[0], move[1]);
            // Sa fitness change en conséquence.
            solution.fitness += delta[move[0]][move[1]];
        }
        // On retourne la meilleure solution.
        return solution;
    }
}