package algo;

import evaluation.Solution;

/** Classe abstraite Search. */
public abstract class Search {
    /** Dimension n de la solution. */
    protected int n;
    /** Stocke la durée d'exécution max admis pour un algorithme. */
    protected long time;
    /** Stocke le temps d'exécution max admis pour un algorithme. */
    protected long maxTime;
    /** Ce tableau stock l'ensemble des variation de fitness pour tous les voisins possibles d'une solution. */
    protected double [][] delta;
    /** Petit tableau d'entier pour stocker une permutation. */
    protected int[] move;
    /** Permet de stocker la meilleure variation d'un voisinage. */
    private double bestDelta;
    /** Compteur. */
    protected int occur;

    /** Constructeur */
    public Search(){
        this.n = 0;
        this.time = 0;
        this.delta = new double[this.n][this.n];
        this.move = new int[2];
        this.bestDelta = 0;
        this.maxTime = 0;
        initOccur();
    }

    /** Permet de minimiser une solution selon un algorithme particulier.
     * Methode abstraite.
     *
     * @param solution La solution à minimiser.
     *
     * @return La solution minimisée.
     */
    public abstract Solution compute(Solution solution);

    /** Cette méthode permet de trouver la permutation de solution proposant le meilleur voisin.
     *
     * Elle compare toutes les permutations possibles selon leurs variations de fitness respectives.
     * La meilleure permutation est celle possédant la variation la plus petite.
     *
     * Dans l'idéal, cette variation doit toujours être négative.
     * Si ce n'est pas le cas, cela signifie qu'il n'existe pas de "meilleur voisin" que la solution initiale.
     */
    protected void selectBestNeighbor(){
        this.move[0] = 1;
        this.move[1] = 0;
        bestDelta = this.delta[this.move[0]][this.move[1]];
        for(int i = 1 ; i < this.n ; i++){
            for(int j = 0; j < i ; j++){
                if(this.delta[i][j] < bestDelta){
                    this.move[0] = i;
                    this.move[1] = j;
                    bestDelta = this.delta[i][j];
                }
            }
        }
    }

    /** Initialise le nombre d'occurrences effectués par un algorithme. */
    public void initOccur(){
        this.occur = 0;
    }

    /** Récupère le nombre d'occurrences.
     *
     * @return Le nombre d'occurrences.
     */
    public int getOccur(){return this.occur;}
}