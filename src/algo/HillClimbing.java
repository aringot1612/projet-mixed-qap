package algo;

import evaluation.NeighBorHoodEval;
import evaluation.Solution;

/** Classe prenant en charge l'algorithme Hill Climbing. */
public class HillClimbing extends Search{
    /** Instance d'évaluation de voisinage. */
    private final NeighBorHoodEval neighBorHoodEval;
    /** Booléen optimum local. */
    private boolean localOpt;

    /** Constructeur
     *
     * @param neighBorHoodEval la méthode d'évaluation de voisinage à utiliser. Attention, seules les instances de FullNeighborhoodEval et IncrementalNeighborhoodEval sont admises.
     * @param n La dimension de la solution.
     * @param time Le temps d'exécution max admis pour l'algorithme.
     */
    public HillClimbing(NeighBorHoodEval neighBorHoodEval, int n, int time){
        this.neighBorHoodEval = neighBorHoodEval;
        this.n = n;
        this.delta = new double[this.n][this.n];
        this.time = time;
        this.localOpt = false;
    }

    /** Permet de minimiser la fitness d'une solution en utilisant un algorithme Hill-Climber Best Improvement.
     *
     * @param solution la solution à minimiser.
     *
     * @return La nouvelle solution minimisée.
     */
    @Override
    public Solution compute(Solution solution) {
        // Initialisation.
        initOccur();
        // Booléen : optimum local.
        this.localOpt = false;
        // Analyse complète du voisinage.
        neighBorHoodEval.init(solution, this.delta);
        this.maxTime = System.currentTimeMillis() + this.time;
        // Tant que l'optimum local n'est pas atteint et qu'il reste du temps...
        while(!localOpt && System.currentTimeMillis() < this.maxTime){
            // Cette méthode va récupérer la permutation du meilleur voisin de la solution.
            selectBestNeighbor();
            // Si le voisin est meilleur que la solution initiale...
            if(delta[move[0]][move[1]] < 0){
                // La solution devient son meilleur voisin.
                solution.swap(move[0], move[1]);
                // Sa fitness est mise à jour très facilement car sa variation a deja été calculée.
                solution.fitness += delta[move[0]][move[1]];
                // Juste avant de re-boucler, on analyse le nouveau voisinage. */
                neighBorHoodEval.update(solution, move, delta);
            // Si le meilleur voisin est moins minimisant que la solution...
            }else
                localOpt = true;
            occur++;
        }
        return solution;
    }
}