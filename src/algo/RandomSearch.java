package algo;

import evaluation.*;
import operator.RandomPermutation;

/** Classe prenant en charge l'algorithme Random Search. */
public class RandomSearch extends Search{
    /** Type d'évaluation : Attention, seules les évaluations de type IncrementalNeighborhoodEval ou MixedQAPeval sont admises. */
    private final Object eval;
    /** Instance de RandomPermutation pour les permutations aléatoires. */
    private final RandomPermutation randomPermutation;
    /** Stocke la fitness de solution initiale. */
    private double bestFitness;

    /** Constructeur
     *
     * @param eval La méthode d'évaluation à utiliser. Attention, seules les évaluations de type MixedQAPeval ou IncrementalEval sont admises.
     * @param time Le temps d'exécution max admis pour l'algorithme.
     */
    public RandomSearch(Object eval, long time){
        this.eval = eval;
        this.time = time;
        this.randomPermutation = new RandomPermutation();
        this.bestFitness = 0.0;
    }

    /** Permet de minimiser la fitness d'une solution en utilisant un algorithme de Recherche Aléatoire.
     *
     * @param solution la solution à minimiser.
     *
     * @return La nouvelle solution minimisé.
     */
    @Override
    public Solution compute(Solution solution) {
        // Initialisation.
        initOccur();
        if(this.eval instanceof IncrementalNeighborhoodEval || this.eval instanceof FullNeighborhoodEval){
            System.out.println("Erreur de configuration, le choix de méthode d'évaluation est invalide.");
            return solution;
        }
        // Sauvegarde de la fitness actuelle.
        this.bestFitness = solution.fitness;
        this.maxTime = System.currentTimeMillis() + this.time;
        // Tant qu'il reste du temps pour l'algorithme...
        while (System.currentTimeMillis() < this.maxTime){
            // Récupération d'une permutation aléatoire, non effectuée.
            this.move = randomPermutation.getMove(solution);
            // Swap, puis évaluation complète.
            if(this.eval instanceof MixedQAPeval){
                solution.swap(move[0], move[1]);
                ((MixedQAPeval)this.eval).apply(solution);
            }
            // Evaluation incrémentale puis swap.
            else{
                ((IncrementalEval)this.eval).apply(solution, this.move);
                solution.swap(move[0], move[1]);
            }
            // Si la nouvelle solution est meilleure que la précédente, elle prends sa place.
            if (solution.fitness < bestFitness)
                bestFitness = solution.fitness;
            else{
                // Reprise de la solution initiale.
                solution.fitness = bestFitness;
                solution.swap(move[0], move[1]);
            }
            occur++;
        }
        return solution;
    }
}