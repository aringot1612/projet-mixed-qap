package algo;

import evaluation.FullNeighborhoodEval;
import evaluation.Solution;
import operator.KSwap;

/** Classe prenant en charge l'algorithme ILS avec optimisation continue. */
public class MixedILSAlgorithm extends Search{
    /** Instance de l'opérateur KSwap permettant de faire des permutations multiples sur une solution. */
    private final KSwap perturbation;
    /** Instance de LocalSearch permettant d'effectuer une recherche locale dans une solution. */
    private final LocalSearch localSearch;
    /** Instance de l'algorithme 1 + 1 ES. */
    private final OneAndOneES oneAndOneES;
    /** Sauvegarde une liste sigma, permettant de garder la meilleure liste rencontrée. */
    private final int[] sigmaBackup;
    /** Sauvegarde une liste x, permettant de garder la meilleure liste de x rencontrée. */
    private final double[] xBackup;
    /** Sauvegarde la meilleure fitness rencontrée. */
    private double bestFitness;


    /** Constructeur
     *
     * @param neighBorHoodEval Evaluation de voisinage.
     * @param oneAndOneES Instance de l'algorithme 1 + 1 ES.
     * @param n La dimension de la solution.
     * @param time Le temps d'exécution max admis pour l'algorithme.
     * @param kSwap L'instance de KSwap permettant de réaliser des perturbations sur une solution.
     */
    public MixedILSAlgorithm(FullNeighborhoodEval neighBorHoodEval, OneAndOneES oneAndOneES, int n, long time, KSwap kSwap){
        this.localSearch = new LocalSearch(neighBorHoodEval, n);
        this.oneAndOneES = oneAndOneES;
        this.n = n;
        this.time = time;
        this.perturbation = kSwap;
        this.sigmaBackup = new int[this.n];
        this.xBackup = new double[this.n];
        this.bestFitness = 0;
    }

    /** Permet de minimiser la fitness d'une solution en utilisant un algorithme Iterated Local Search couplé à un 1 + 1 ES.
     *
     * @param solution la solution à minimiser.
     *
     * @return La nouvelle solution minimisée.
     */
    @Override
    public Solution compute(Solution solution) {
        // Initialisation
        initOccur();
        // Application d'une recherche locale sur la solution.
        this.localSearch.compute(solution);
        this.maxTime = System.currentTimeMillis() + this.time;
        // Sauvegarde de la solution, on considère qu'il s'agit, pour le moment, de la meilleure.
        saveSolution(solution);
        // Tant que le temps maximum n'est pas encore atteint...
        while (System.currentTimeMillis() < this.maxTime){
            // Lancement d'une perturbation aléatoire sur la solution.
            perturbation.run(solution);
            // Recherche locale sur la solution clonée.
            this.localSearch.compute(solution);
            // Appel à l'algorithme 1 + 1 ES pour l'optimisation continue.
            if(Math.random() < 0.5)
                solution = oneAndOneES.compute(solution);
            // Si la meilleure solution fournie est meilleure que la solution initiale...
            if (solution.fitness < this.bestFitness)
                // On sauvegarde la nouvelle meilleure solution.
                saveSolution(solution);
            else
                // Sinon on récupère la meilleure solution enregistrée.
                restoreSolution(solution);
            occur++;
        }
        return solution;
    }

    /** Cette méthode permet de sauvegarder l'état d'une solution fournie en paramètre.
     *
     * Elle enregistre la liste sigma et la liste des x de la solution donnée dans des listes spécifiques.
     * Elle se charge également de sauvegarder la fitness.
     *
     * @param solution La solution à restaurer.
     */
    private void saveSolution(Solution solution){
        for(int i = 0 ; i < n; i++){
            sigmaBackup[i] = solution.sigma[i];
            xBackup[i] = solution.x[i];
        }
        this.bestFitness = solution.fitness;
    }

    /** Cette méthode permet de restaurer une solution fournie en paramètre.
     *
     * Elle restaure la liste des sigma et la liste des x en utilisant des listes de backup.
     * Elle restaure également la fitness.
     *
     * @param solution La solution nécessitant une restauration.
     */
    private void restoreSolution(Solution solution){
        for(int i = 0 ; i < n; i++){
            solution.sigma[i] = sigmaBackup[i];
            solution.x[i] = xBackup[i];
        }
        solution.fitness =  this.bestFitness;
    }
}