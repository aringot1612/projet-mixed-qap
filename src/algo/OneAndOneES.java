package algo;

import evaluation.MixedQAPeval;
import evaluation.Solution;

/** Classe prenant en charge l'algorithme 1 + 1 ES. */
public class OneAndOneES extends Search
{
    /** Instance d'évaluation de solution. */
    private final MixedQAPeval eval;
    /** Stock la valeur sigma. */
    private double sigma;
    /** Stock la valeur gamma. */
    private final double gamma;
    /** Stock le nombre de variations minimes, enchainé par l'algorithme. */
    private int minorChangesCounter;
    /** Stock la dernière fitness obtenue. */
    private double lastFitness;
    /** Sauvegarde la liste des x d'une solution. */
    private final double[] xBackup;
    /** Sauvegarde la fitness d'une solution. */
    private double fitnessBackup;

    /** Constructeur
     *
     * @param eval L'évaluation de solution à utiliser.
     * @param sigma Sigma paramétrable.
     * @param gamma Gamma paramétrable.
     */
    public OneAndOneES(MixedQAPeval eval, double sigma, double gamma)
    {
        this.eval = eval;
        this.sigma = sigma;
        this.gamma = gamma;
        this.minorChangesCounter = 0;
        this.lastFitness = 0.0;
        this.xBackup = new double[this.eval.n];
        this.fitnessBackup = 0.0;
    }

    /** Permet de minimiser une solution en modifiant les valeurs de x : Optimisation continue.
     *
     * @param solution La solution à minimiser.
     *
     * @return la solution minimisée.
     */
    @Override
    public Solution compute(Solution solution) {
        // Initialisation.
        initOccur();
        // On sauvegarde la dernière fitness atteinte.
        lastFitness = solution.fitness;
        // Backup de la solution pour éviter le clonage.
        saveSolution(solution);
        // On met le compteur de variations minimes à 0.
        minorChangesCounter = 0;
        // Tant qu'il y a du progrès à réaliser...
        while(minorChangesCounter < 200)
        {
            // Récupération de la dernière solution enregistrée.
            restoreSolution(solution);
            // Pour chaque x
            for(int i=0;i<solution.x.length;i++)
            {
                // En moyenne, une composante est modifiée
                if(Math.random() < 1.0/solution.x.length)
                {
                    // Variation négative ou positive
                    if(Math.random() < 0.5)
                    {
                        solution.x[i] = solution.x[i] - sigma * Math.random();
                    }
                    else
                    {
                        solution.x[i] = solution.x[i] + sigma * Math.random();
                    }
                }
            }

            // Reparation de la solution.
            repair(solution.x);
            solution.modifiedX = true;

            // Evaluation de la solution.
            this.eval.apply(solution);

            // Si la nouvelle solution est meilleur que la fitness sauvegardée...
            if(solution.fitness < fitnessBackup)
            {
                // Sauvegarde de la solution pour prochaine itération.
                saveSolution(solution);
                // Maj du sigma
                sigma = sigma * gamma;
            }
            else
            {
                sigma = sigma * Math.pow(gamma,(-1.0/4.0));
            }

            // Mise à jour du compteur de variation minime.
            if(Math.abs(lastFitness - solution.fitness) < 0.000001)
                minorChangesCounter++;
            else
                minorChangesCounter = 0;

            // On met à jour la dernière fitness atteinte. */
            lastFitness = solution.fitness;
            occur++;
        }
        restoreSolution(solution);
        return solution;
    }

    /** Permet d'appliquer une réparation sur les valeurs de x d'une solution.
     *
     * Cette méthode est appelée à chaque itération,
     * Elle se charge de vérifier les contraintes suivantes :
     *
     * Toute composante de x est positive et la somme des composantes de x doit être égale à 1.
     *
     * @param x L'ensemble x à vérifier.
     */
    public void repair(double[] x)
    {
        double somme = 0.0;

        // Réparation 1.
        for(int i=0;i<x.length;i++)
        {
            if(x[i] < 0)
            {
                x[i] = -x[i];
            }

            somme += x[i];
        }

        // Réparation 2.
        if(somme == 0.0)
        {
            for(int i=0;i<x.length;i++)
            {
                x[i] = 1.0/x.length;
            }
        }
        // Réparation 3.
        else if(somme != 1.0)
        {
            for(int i=0;i<x.length;i++)
            {
                x[i] = x[i] / somme;
            }
        }
    }

    /** Cette méthode permet de sauvegarder l'état d'une solution fournie en paramètre.
     *
     * Elle enregistre la liste des x de la solution donnée dans une liste spécifique.
     * Elle se charge également de sauvegarder la fitness.
     *
     * @param solution La solution à restaurer.
     */
    private void saveSolution(Solution solution){
        if (this.eval.n >= 0) System.arraycopy(solution.x, 0, xBackup, 0, this.eval.n);
        this.fitnessBackup = solution.fitness;
    }

    /** Cette méthode permet de restaurer une solution fournie en paramètre.
     *
     * Elle restaure la liste des x en utilisant une liste de backup.
     * Elle restaure également la fitness.
     *
     * @param solution La solution nécessitant une restauration.
     */
    private void restoreSolution(Solution solution){
        if (this.eval.n >= 0) System.arraycopy(xBackup, 0, solution.x, 0, this.eval.n);
        solution.fitness =  this.fitnessBackup;
    }
}