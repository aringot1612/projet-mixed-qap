package executables;

import algo.HillClimbing;
import operator.Runner;

/** Executable pour l'algorithme Hill Climbing. */
public class HillClimbingRunner extends Runner {
    public static void main(String[] args){
        // INPUT 1 : Nom d'instance.
        init("instances/mixedQAP_uni_20_1_100_1.dat");

        // INPUT 2 : Temps d'exécution max admis pour cet algorithme.
        int maxTime = 1000;

        /** INPUT 3 :
         * Ici : le premier paramètre peut être modifié :
         *
         * - fullNeighborhoodEval : Evaluation de voisinage basique (dite complète) -> Par défaut.
         * - incrementalNeighborhoodEval : Evaluation de voisinage améliorée (dite incrémentale). */
        HillClimbing hillClimbing = new HillClimbing(fullNeighborhoodEval, mixedQAPeval.n, maxTime);

        /** INPUT 4 : Optimisation continue sur la solution avec un algorithme 1+1 ES :
         * true : activée
         * false : désactivée. -> Par défaut.
         *
         * Personalisation du sigma et gamma du 1 + 1 ES :
         * La ligne suivante doit être ajoutée :
         *
         * initOneAndOneES(0.9, 2);
         *
         * Paramètre 1 : sigma
         * Paramètre 2 : gamma
         * */
        boolean onePlusOne = false;

        solution = prepare(solution, uc, onePlusOne);
        mixedQAPeval.apply(solution);
        solution = hillClimbing.compute(solution);
        mixedQAPeval.apply(solution);
        System.out.println("Nombre de passage dans le while principal : " + hillClimbing.getOccur());
        System.out.println("Solution : " + solution);
    }
}