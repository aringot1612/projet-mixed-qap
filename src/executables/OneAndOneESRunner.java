package executables;

import algo.OneAndOneES;
import operator.Runner;

/** Executable pour l'algorithme 1 + 1 ES. */
public class OneAndOneESRunner extends Runner {
    public static void main(String[] args){
        // INPUT 1 : Nom d'instance.
        init("instances/mixedQAP_uni_20_1_100_1.dat");

        // INPUT 2 : Nombre de simulation à réaliser pour cet algorithme.
        int nbSimulation = 30;

        // INPUT 3 : Sigma pour Le 1 + 1 ES
        double sigma = 0.9;

        // INPUT 4 : Gamma pour Le 1 + 1 ES
        int gamma = 2;

        for(int i = 0 ; i < nbSimulation ; i++){
            OneAndOneES oneAndOneES = new OneAndOneES(mixedQAPeval, sigma, gamma);
            System.out.println("Simulation " + (i+1) + " / " + nbSimulation + " en cours...");
            uc.run(solution);
            mixedQAPeval.apply(solution);
            solution = oneAndOneES.compute(solution);
            mixedQAPeval.apply(solution);
            sumFitness += solution.fitness;
            sumOccur += oneAndOneES.getOccur();
        }
        System.out.println("Dernière solution : " + solution);
        System.out.println("Nombre de passage dans le while principal en moyenne: " + (sumOccur/nbSimulation));
        System.out.println("Moyenne de fitness : " + (sumFitness/nbSimulation));
    }
}