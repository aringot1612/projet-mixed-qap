package executables;

import algo.RandomSearch;
import operator.Runner;

/** Executable pour l'algorithme Random Search. */
public class RandomSearchRunner extends Runner {
    public static void main(String[] args){
        // INPUT 1 : Nom d'instance.
        init("instances/mixedQAP_uni_20_1_100_1.dat");

        // INPUT 2 : Nombre de simulation à réaliser pour cet algorithme.
        int nbSimulation = 30;

        // INPUT 3 : Temps d'exécution max admis pour cet algorithme.
        int maxTime = 1000;

        /** INPUT 4 :
         * Ici : le premier paramètre peut être modifié :
         *
         * - mixedQAPeval : Evaluation basique (dite complète) -> Par défaut.
         * - incrementalEval : Evaluation améliorée (dite incrémentale). */
        RandomSearch randomSearch = new RandomSearch(mixedQAPeval, maxTime);

        /** INPUT 5 : Optimisation continue sur la solution avec un algorithme 1+1 ES :
         * true : activée
         * false : désactivée. -> Par défaut.
         *
         * Personalisation du sigma et gamma du 1 + 1 ES :
         * La ligne suivante doit être ajoutée :
         *
         * initOneAndOneES(0.9, 2);
         *
         * Paramètre 1 : sigma
         * Paramètre 2 : gamma
         * */
        boolean onePlusOne = false;

        for(int i = 0 ; i < nbSimulation ; i++){
            System.out.println("Simulation " + (i+1) + " / " + nbSimulation + " en cours...");
            solution = prepare(solution, uc, onePlusOne);
            mixedQAPeval.apply(solution);
            solution = randomSearch.compute(solution);
            mixedQAPeval.apply(solution);
            sumFitness += solution.fitness;
            sumOccur += randomSearch.getOccur();
        }
        System.out.println("Dernière solution : " + solution);
        System.out.println("Nombre de passage dans le while principal en moyenne: " + (sumOccur/nbSimulation));
        System.out.println("Moyenne de fitness : " + (sumFitness/nbSimulation));
    }
}