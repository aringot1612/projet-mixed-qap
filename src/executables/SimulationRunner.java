package executables;

import operator.Simulation;

/** Executable pour la simulation d'algorithme. */
public class SimulationRunner {
    public static void main(String[] args){
        Simulation completeSimulation = new Simulation(30, new int[]{10, 100, 1000, 2000});
        completeSimulation.execute();
    }
}