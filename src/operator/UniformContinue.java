package operator;

import evaluation.Solution;

/** Classe utilitaire pour gérer une initialisation de solution. */
public class UniformContinue {
    /** Dimension de la solution. */
    private final int n;

    /** Constructeur
     *
     * @param n La dimension de la solution.
     */
    public UniformContinue(int n){this.n = n;}

    /** Permet d'initialiser une solution rapidement.
     *
     * @param solution la solution (présumée vide).
     */
    public void run(Solution solution) {
        solution.resize(n);
        solution.resizeSigma(n);
        for(int i = 0; i < n; i++){
            solution.x[i] =  1.0 / n;
            solution.sigma[i] = i;
        }
        solution.modifiedX = true;
    }
}