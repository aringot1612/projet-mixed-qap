package operator;

import evaluation.Solution;
import java.util.Random;

/** Classe utilitaire pour gérer une permutation aléatoire. */
public class RandomPermutation {
    /** Instance de type Random pour gérer l'aléatoire. */
    private final Random random;
    /** Stocke la permutation effectuée. */
    private int[] move;

    /** Constructeur */
    public RandomPermutation(){
        this.random = new Random();
        this.move = new int[2];
    }

    /** Permet d'appliquer une permutation aléatoire sur la solution fournie en paramètre.
     *
     * @param s La solution devant subir une permutation aléatoire.
     *
     * @return La permutation réalisée.
     */
    public int[] run(Solution s) {
        move = getMove(s);
        s.swap(move[0],move[1]);
        return move;
    }

    /** Permet de récupérer une permutation valide sans l'exécuter.
     *
     * @param s La solution devant subir une permutation aléatoire.
     *
     * @return La permutation trouvée.
     */
    public int[] getMove(Solution s){
        move[0] = random.nextInt(s.sigma.length);
        move[1] = random.nextInt(s.sigma.length);
        while(move[0] == move[1])
            move[1] = random.nextInt(s.sigma.length);
        if(move[0] > move[1]){
            int tmp = move[0];
            move[0] = move[1];
            move[1] = tmp;
        }
        return move;
    }
}