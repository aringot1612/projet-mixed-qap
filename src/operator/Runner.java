package operator;

import algo.OneAndOneES;
import evaluation.*;
import java.io.FileNotFoundException;

/** Classe utilitaire pour gérer les executables. */
public class Runner {
    protected static MixedQAPeval mixedQAPeval;
    protected static IncrementalEval incrementalEval;
    protected static UniformContinue uc;
    protected static KSwap kSwap;
    protected static FullNeighborhoodEval fullNeighborhoodEval;
    protected static IncrementalNeighborhoodEval incrementalNeighborhoodEval;
    protected static Solution solution;
    protected static OneAndOneES oneAndOneES;
    protected static double sumFitness = 0.0;
    protected static int sumOccur = 0;

    protected static void init(String instanceName){
        try{
            mixedQAPeval = new MixedQAPeval(instanceName);
            incrementalEval = new IncrementalEval(mixedQAPeval);
            uc = new UniformContinue(mixedQAPeval.n);
            kSwap = new KSwap(mixedQAPeval);
            fullNeighborhoodEval = new FullNeighborhoodEval(mixedQAPeval);
            incrementalNeighborhoodEval = new IncrementalNeighborhoodEval(incrementalEval);
            solution = new Solution(mixedQAPeval.n);
            oneAndOneES = new OneAndOneES(mixedQAPeval, 0.9, 2.0);
            sumFitness = 0.0;
            sumOccur = 0;
        }catch(FileNotFoundException e){
            System.out.println(e.getMessage());
        }
    }

    protected static void initOneAndOneES(double sigma, int gamma){
        oneAndOneES = new OneAndOneES(mixedQAPeval, sigma, gamma);
    }

    protected static Solution prepare(Solution solution, UniformContinue uc, boolean onePlusOneESActivated){
        uc.run(solution);
        if(onePlusOneESActivated)
            solution = oneAndOneES.compute(solution);
        return solution;
    }
}