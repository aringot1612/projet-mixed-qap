package operator;

import evaluation.IncrementalEval;
import evaluation.MixedQAPeval;
import evaluation.NeighBorHoodEval;
import evaluation.Solution;
import java.util.Random;

/** Classe utilitaire pour gérer les k permutations aléatoires. */
public class KSwap{
    /** Nombre de permutations aléatoires que cette instance doit réaliser sur une solution. */
    private int k;
    /** Borne inférieure pour trouver k. */
    private int kMin;
    /** Borne inférieure pour trouver k. */
    private int kMax;
    /** Instance de RandomPermutation permettant de réaliser une permutation aléatoire. */
    private final RandomPermutation randomPermutation;
    /** Instance de type Random pour gérer l'aléatoire. */
    private final Random random;
    /** Objet qui sera utilisé pour évaluer la solution après permutation. Le type de d'évaluation est libre de choix. */
    private final Object eval;
    /** Stocke les permutations effectuées. */
    private int[][] moves;

    /** Constructeur
     *
     * @param eval Type d'évaluation à utiliser. Types acceptés : IncrementalNeighborhoodEval OU MixedQAPeval.
     */
    public KSwap(Object eval){
        this.randomPermutation = new RandomPermutation();
        this.eval = eval;
        this.random = new Random();
        this.kMin = 1;
        this.kMax = 1;
        this.k = 1;
        this.moves = new int[0][0];
    }

    /** Méthode permettant d'appliquer k permutations aléatoires sur une solution s.
     *
     * Si l'une des méthodes d'évaluation suivante est fournie : IncrementalNeighborhoodEval OU MixedQAPeval.
     * La solution sera directement évaluée.
     *
     * @param s La solution devant subir k permutations aléatoires.
     */
    public void run(Solution s) {
        // Borne max aléatoire entre 1 et sigma.length/2.
        kMax = random.nextInt(s.sigma.length/2) + 1;
        // Borne min aléatoire entre 1 et borne max.
        kMin = random.nextInt(kMax) + 1;
        // Nombre de permutation aléatoire entre bornes.
        k = random.nextInt(kMax) + kMin;
        // Stocke les permutations réalisées.
        moves = new int[k][2];
        // Pour chaque permutation à réaliser.
        for(int i = 0 ; i < k ; i++){
            // On récupère une permutation aléatoire (sans execution).
            moves[i] = this.randomPermutation.getMove(s);
            // Si l'évaluation incrémentale est utilisée, on évalue en premier.
            if(this.eval instanceof IncrementalEval)
                ((NeighBorHoodEval) this.eval).update(s, moves[i], null);
            // On réalise la permutation.
            s.swap(moves[i][0], moves[i][1]);
        }
        // On évalue la solution en cas d'évaluation complète en fin d'algorithme.
        if(this.eval instanceof MixedQAPeval)
            ((MixedQAPeval) this.eval).apply(s);
    }
}