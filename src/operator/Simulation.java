package operator;

import algo.*;
import evaluation.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/** Classe utilitaire pour gérer une simulation d'algorithmes. */
public class Simulation {
    private final String [] instancesNames;
    private final int nbSimulation;
    private MixedQAPeval mixedQAPeval;
    private IncrementalEval incrementalEval;
    private FullNeighborhoodEval fullNeighborhoodEval;
    private IncrementalNeighborhoodEval incrementalNeighborhoodEval;
    private UniformContinue uc;
    private KSwap kSwap;
    private double sumFitness;
    private int sumOccurs;
    private final int[] executionsTime;
    private Solution solution;
    private FileWriter writer;

    public Simulation(int nbSimulation, int[] executionsTime) {
        File directory = new File("./instances");
        this.instancesNames = directory.list();
        try{
            System.out.println(this.instancesNames[0]);
            this.mixedQAPeval = new MixedQAPeval("./instances/" + this.instancesNames[0]);
            this.writer=new FileWriter("./instancesResults/" + this.instancesNames[0]);
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
        this.nbSimulation = nbSimulation;
        this.executionsTime = executionsTime;
        init();
    }

    public void execute(){
        for(String instanceName : this.instancesNames){
            String instanceNameCsv = "./instancesResults/"+ instanceName;
            try{
                File myObj = new File(instanceNameCsv);
                if (myObj.createNewFile()) {
                    System.out.println("File created: " + myObj.getName());
                } else {
                    if(myObj.delete())
                        if(!myObj.createNewFile()){
                            System.out.println("An error occurred.");
                            return;
                        }
                }
                writer = new FileWriter(instanceNameCsv);
                this.mixedQAPeval = new MixedQAPeval("./instances/" + instanceName);
                for(int executionTime : executionsTime){
                    System.out.println("Instance : " + instanceName + " | Simulation de " + executionTime + " milli-secondes pour chaque algorithme.");
                    writer.write("\nDurée max pour chaque algorithme : " + (executionTime) + " milli-secondes.");
                    RandomSearch randomSearch = new RandomSearch(mixedQAPeval, executionTime);
                    writer.write("\n\n\nRandomSearch selon une évaluation complète");
                    runMultiple(randomSearch);

                    randomSearch = new RandomSearch(incrementalEval, executionTime);
                    writer.write("\n\nRandomSearch selon une évaluation incrémentale.");
                    runMultiple(randomSearch);

                    ILS ils = new ILS(fullNeighborhoodEval, mixedQAPeval.n, executionTime, kSwap);
                    writer.write("\n\nILS selon une évaluation complète.");
                    runMultiple(ils);

                    ils = new ILS(incrementalNeighborhoodEval, mixedQAPeval.n, executionTime, kSwap);
                    writer.write("\n\nILS selon une évaluation incrémentale.");
                    runMultiple(ils);

                    HillClimbing hillClimbing = new HillClimbing(fullNeighborhoodEval, mixedQAPeval.n, executionTime);
                    writer.write("\n\nHillClimbing selon une évaluation complète.");
                    run(hillClimbing);

                    hillClimbing = new HillClimbing(incrementalNeighborhoodEval, mixedQAPeval.n, executionTime);
                    writer.write("\n\nHillClimbing selon une évaluation incrémentale.");
                    run(hillClimbing);

                    writer.write("\n\n\n\n\n");
                }
                writer.close();
            }catch(IOException e){
                System.out.println(e.getMessage());
            }
        }
    }

    private void runMultiple(Object algorithm) throws IOException {
        init();
        for(int i = 0 ; i < nbSimulation ; i++){
            uc.run(solution);
            mixedQAPeval.apply(solution);
            solution = ((Search)algorithm).compute(solution);
            mixedQAPeval.apply(solution);
            this.sumFitness += solution.fitness;
            this.sumOccurs += ((Search)algorithm).getOccur();
        }
        writer.write("\nFitness en moyenne : " + (this.sumFitness/nbSimulation));
        writer.write("\nNombre de passage dans le while principal en moyenne : " + (this.sumOccurs/nbSimulation));
    }

    private void run(Object algorithm) throws IOException {
        init();
        uc.run(solution);
        mixedQAPeval.apply(solution);
        solution = ((Search)algorithm).compute(solution);
        mixedQAPeval.apply(solution);
        writer.write("\nFitness : " + solution.fitness);
        writer.write("\nNombre de passage dans le while principal : " + ((Search)algorithm).getOccur());
    }

    private void init(){
        this.sumFitness = 0.0;
        this.sumOccurs = 0;
        this.incrementalEval = new IncrementalEval(mixedQAPeval);
        this.fullNeighborhoodEval = new FullNeighborhoodEval(mixedQAPeval);
        this.incrementalNeighborhoodEval = new IncrementalNeighborhoodEval(incrementalEval);
        this.uc = new UniformContinue(mixedQAPeval.n);
        this.kSwap = new KSwap(mixedQAPeval);
        this.solution = new Solution(this.mixedQAPeval.n);
    }
}