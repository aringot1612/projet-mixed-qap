package deprecated;

import evaluation.MixedQAPeval;
import evaluation.Solution;

/** WARNING : DOESN'T WORK.
 * @deprecated  */
@Deprecated
public class IncrementalEvalImproved {
    /** Instance de la méthode d'évaluation complète, obligatoire pour le fonctionnement de cette méthode d'évaluation. */
    private final MixedQAPeval eval;
    /** Stocke une somme de valeurs issue de la formule appliquée. */
    private double sum;
    /** (u-v) -> Permutation et k -> Index pour les boucles for. */
    private int u, v, newU, newV, k;
    private double fitnessVariation, fitnessSecondVariation;

    /** Constructeur
     *
     * @param eval L'instance d'évaluation complète à utiliser.
     */
    public IncrementalEvalImproved(MixedQAPeval eval){
        this.eval = eval;
        this.u = -1;
        this.v = -1;
        this.sum = 0.0;
        this.fitnessVariation = 0.0;
        this.fitnessSecondVariation = 0.0;
        this.newU = -1;
        this.newV = -1;
        this.k = 0;
    }

    /** Cette méthode permet d'évaluer une solution issue du voisinage d'une solution initiale.
     *
     * Pour que cette évaluation fonctionne correctement,
     * Il faut impérativement fournir la solution initiale, et non le voisin.
     * Il faut également fournir la permutation de solution responsable du voisin à évaluer.
     *
     * @param solution Solution initiale à utiliser pour évaluer la solution voisine.
     * @param move Permutation qui, appliquée à la solution initiale, donne le voisin à évaluer.
     */
    public void apply(Solution solution, int[] move) {
        if(this.u == -1 || this.v == -1 || this.u == move[1] || this.v == move[0] || this.u == move[0] ||  this.v == move[1]){
            if(move[0] < move[1]){
                u = move[0];
                v = move[1];
            }
            else{
                u = move[1];
                v = move[0];
            }
            sum = 0.0;
            for(k = 0 ; k < u ; k++){
                sum += ((solution.flow[u][k] - solution.flow[v][k])*(this.eval.distance[solution.sigma[v]][solution.sigma[k]] - this.eval.distance[solution.sigma[u]][solution.sigma[k]]));
            }
            for(k = u+1 ; k < v ; k++){
                sum += ((solution.flow[k][u] - solution.flow[v][k])*(this.eval.distance[solution.sigma[v]][solution.sigma[k]] - this.eval.distance[solution.sigma[u]][solution.sigma[k]]));
            }
            for(k = v+1 ; k < this.eval.n ; k++) {
                sum += ((solution.flow[k][u] - solution.flow[k][v]) * (this.eval.distance[solution.sigma[v]][solution.sigma[k]] - this.eval.distance[solution.sigma[u]][solution.sigma[k]]));
            }
            this.fitnessVariation = 2*sum;
            solution.fitness = solution.fitness + this.fitnessVariation ;
            solution.swap(u, v);
        }
        else{
            if(move[0] < move[1]){
                newU = move[0];
                newV = move[1];
            }
            else{
                newU = move[1];
                newV = move[0];
            }
            solution.swap(newU, newV);
            fitnessSecondVariation =
                    2 *      (solution.flow[u][newU]
                            - solution.flow[v][newU]
                            - solution.flow[u][newV]
                            + solution.flow[v][newV])
                    *    (this.eval.distance[solution.sigma[v]][solution.sigma[newU]]
                        - this.eval.distance[solution.sigma[u]][solution.sigma[newU]]
                        - this.eval.distance[solution.sigma[v]][solution.sigma[newV]]
                        + this.eval.distance[solution.sigma[u]][solution.sigma[newV]]);
            this.fitnessVariation = this.fitnessVariation + fitnessSecondVariation;
            solution.fitness = solution.fitness + this.fitnessVariation;
            this.u = newU;
            this.v = newV;
            Solution clone = solution.clone();
            this.eval.apply(clone);
            System.out.println("Diff : " + (solution.fitness - clone.fitness));
        }
    }
}